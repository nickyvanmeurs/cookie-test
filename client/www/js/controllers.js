angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $http) {
  $scope.cookie = null;
  $scope.status = null;

  var gotError = function(error) {
    var errMsg = (error) ? error : 'unknown error';
    $scope.status = 'ERROR: ' + errMsg;
  };

  var cookieMasterNotDefined = function() {
    $scope.status = 'ERROR: cookieMaster not defined';
  };

  $scope.grabCookie = function() {
    $scope.status = 'Grabbing cookie from localhost';

    $http.get('https://localhost:5000/set-cookie', { withCredentials: true }).then(function() {
      $scope.status = 'Grabbed cookie from localhost';
    });
  };

  $scope.readCookie = function() {
    $scope.status = 'reading cookie';

    if (!window.cookieMaster) {
      cookieMasterNotDefined();
      return;
    }

    var gotCookie = function(data) {
      $scope.cookie = data.cookieValue;
      $scope.status = 'read cookie';
    };

    window.cookieMaster.readCookie('localhost', 'my-cookie', gotCookie, gotError);
  };

  $scope.deleteCookies = function() {
    $scope.status = 'deleting cookies';

    if (!window.cookieMaster) {
      cookieMasterNotDefined();
      return;
    }

    var deletedCookies = function() {
      $scope.status = 'deleted all cookies';
    };

    window.cookieMaster.clearCookies(deletedCookies, gotError);
  };
});
