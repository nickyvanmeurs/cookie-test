from flask import Flask
from flask import make_response
from flask.ext.cors import CORS
# from OpenSSL import SSL

# context = SSL.Context(SSL.SSLv23_METHOD)
# context.use_privatekey_file('yourserver.key')
# context.use_certificate_file('yourserver.crt')

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return 'Hello world'

@app.route('/set-cookie')
def set_cookie():
    response = make_response('Let me set this cookie for you.')
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.set_cookie('my_cookie', 'my_cookie_value')
    return response

if __name__ == '__main__':
    app.run(ssl_context='adhoc')
